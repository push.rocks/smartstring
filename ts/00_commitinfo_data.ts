/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartstring',
  version: '4.0.7',
  description: 'handle strings in smart ways. TypeScript ready.'
}
