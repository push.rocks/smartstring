// node native
import * as smartenv from '@pushrocks/smartenv';
const smartenvInstance = new smartenv.Smartenv();
import * as isounique from '@pushrocks/isounique';

export { isounique };

import { Buffer } from 'buffer';
if (smartenvInstance.isBrowser) {
  globalThis.Buffer = Buffer;
}
import * as url from 'url';
export { url };

// third party
import { Base64 as jsBase64 } from 'js-base64';

import stripIndent from 'strip-indent';
import normalizeNewline from 'normalize-newline';
import randomatic from 'randomatic';

export { jsBase64, stripIndent, normalizeNewline, randomatic };
