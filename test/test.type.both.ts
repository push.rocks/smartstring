import { tap, expect } from '@pushrocks/tapbundle';
import * as smartstring from '../ts/index.js';

tap.test('should state valuid utf8', async () => {
  expect(smartstring.type.isUtf8('hithere')).toBeTrue();
});

tap.test('should state wether base64 string is valid', async () => {
  const base64String = smartstring.base64.encode('hi there');
  expect(smartstring.type.isBase64(base64String)).toBeTrue();
  expect(smartstring.type.isBase64('hi there')).toBeFalse();
});

tap.start();
