import * as smartstring from '../ts/index.js';
import { tap, expect } from '@pushrocks/tapbundle';

// Domain
let testDomain: smartstring.Domain;
let testDomain2: smartstring.Domain;
tap.test('expect create a new Domain object', async () => {
  testDomain = new smartstring.Domain('https://level3D.level2D.level1D');
  expect(testDomain).toBeInstanceOf(smartstring.Domain);
  console.log(testDomain);
});
tap.test('expect have a .topLevel', async () => {
  expect(testDomain.topLevel).toEqual('level1D');
});
tap.test('expect have a .level2', async () => {
  expect(testDomain.level2).toEqual('level2D');
});
tap.test('expect have a .level3', async () => {
  expect(testDomain.level3).toEqual('level3D');
});
tap.test('expect have the correct dns zone name', async () => {
  expect(testDomain.zoneName).toEqual('level2D.level1D');
});
tap.test('expect have the correct protocol', async () => {
  expect(testDomain.protocol).toEqual('https');
});
tap.test('testDomain2 expect be a basic domain', async () => {
  testDomain2 = new smartstring.Domain('testing.bleu.de');
  console.log(testDomain2);
});

tap.test('should parse complex domains', async () => {
  testDomain2 = new smartstring.Domain('https://sub1.sub2.lossless.com/some/path:5431');
  console.log(testDomain2);
});

tap.start();
