import * as smartstring from '../ts/index.js';
import { tap, expect } from '@pushrocks/tapbundle';

// git
let testGit: smartstring.GitRepo;
tap.test('expect create a new Git class GitRepo', async () => {
  testGit = new smartstring.GitRepo('git+https://github.com/pushrocks/smartstring.git');
  expect(testGit).toBeInstanceOf(smartstring.GitRepo);
});
tap.test('expect return a .host', async () => {
  expect(testGit.host).toEqual('github.com');
});
tap.test('expect return a .user', async () => {
  expect(testGit.user).toEqual('pushrocks');
});
tap.test('expect return a .repo', async () => {
  expect(testGit.repo).toEqual('smartstring');
});
tap.test('expect return a .httpsUrl', async () => {
  expect(testGit.httpsUrl).toEqual('https://github.com/pushrocks/smartstring.git');
});
tap.test('expect return a .sshUrl', async () => {
  expect(testGit.sshUrl).toEqual('git@github.com:pushrocks/smartstring.git');
});

tap.start();
